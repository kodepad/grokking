package sw;

public class sw2_MaxSum {

//    Given an array of positive numbers and a positive number ‘k,’ find the maximum sum of any contiguous subarray of size ‘k’.


    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5,6};
        int[] arr2 = {2, 3, 4, 1, 5};
        int[] arr3 = {2, 1, 5, 1, 3, 2};


        int k1 = 3;
        int k2 = 2;
        int k3 = 3;

        System.out.println(findMaxSum(arr1, k1));
        System.out.println(findMaxSum_bruteForce(arr1, k1));

        System.out.println(findMaxSum(arr2, k2));
        System.out.println(findMaxSum_bruteForce(arr2, k2));

        System.out.println(findMaxSum(arr3, k3));
        System.out.println(findMaxSum_bruteForce(arr3, k3));
    }

    public static int findMaxSum(int[] arr, int k){
        int maxSum = Integer.MIN_VALUE;
        int sum = 0;
        int startIndex = 0;

        for(int endIndex = 0; endIndex < arr.length; endIndex++){
            sum += arr[endIndex];       // add element at RHS of window (to f(x)

            if(endIndex >= k-1) // breaking condition, where to create new window
            {
                maxSum = Math.max(maxSum,sum);  // f(x)
                sum = sum - arr[startIndex];    // remove the element at LHS of window from f(x)
                startIndex++;                   // always move starting position at the end of logic
            }
        }
    return maxSum;
    }


    public static int findMaxSum_bruteForce(int [] arr, int k){
        int maxSum = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length - k + 1; i++) {
            int sum = 0;
            for (int j = i; j < i + k ; j++) {
                sum = sum + arr[j];
            }
            maxSum = Math.max(sum, maxSum);
        }
        return maxSum;
    }
}
