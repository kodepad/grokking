package sw;

/*
Given an array, find the average of all contiguous subarrays of size ‘K’ in it.
 */

import java.util.Arrays;

public class sw1_ContiguousSubArrayAverage {

    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4, 5};
        int k = 2;

        double[] result1 = findAverageOfSubarray_bruteForce(arr, k);
        double[] result2 = findAverageOfSubarray(arr, k);

        System.out.println(Arrays.toString(result1));
        System.out.println(Arrays.toString(result2));
    }


    public static double[] findAverageOfSubarray(int[] arr, int k) {

        double[] result = new double[arr.length - k + 1];
        double sum = 0;
        int startIndex = 0;

        for (int endIndex = 0; endIndex < arr.length; endIndex++) {
            sum = sum + arr[endIndex];

     //       if (endIndex - startIndex < k) {
            // Question 1 : why above condition failed
                if(endIndex >= k-1){
                    /* Question 2
                     k - 1 = 1, so what will happen when endIndex moves to 2nd element
                     */
                result[startIndex] = sum / k;
                sum = sum - arr[startIndex];
                startIndex++;

            }
                /*
                Note : Q1 and Q2
                As we are reusing the sum obtained in first subarray, only checking the first window size in if condition will suffice
                then add the incoming element : sum = sum + arr[endIndex]
                remove the outgoing element : sum = sum - arr[startIndex]
                 */

                // Question 3 : How to solve this w/o reusing sum
                // Note : Use another for loop, which will calculate the sum on next window. Have sum = 0 in outer loop, see brute force below
        }
        return result;
    }


    public static double[] findAverageOfSubarray_bruteForce(int[] arr, int k) {

        //  Complexity n*k

        /* Question 1. What should be the length of output array :
            Note : n-k+1

        Once we have reached the last subarray, size of which is k, the length of result array would be n-k
        we need one more index in result array , to store the result of last subarray, hence => n - k + 1
        Always talk in respect to either length or indexes, do not mix them

        */

        double[] avgArray = new double[arr.length - k + 1];
        for (int i = 0; i < avgArray.length; i++) {
            double sum = 0;
            for (int j = i; j < i + k; j++) {
                /* Question : why j < i+k and not j<= i+k
                    Note :
                    as i is index, lets think in term of index.
                    subarray length is k, hence initially, j upper value would be k-1
                    i.e 0 to k-1
                    adding i on both sides
                    // i to i+k-1
                    // hence j < i+k
                 */

                sum = sum + arr[j];

            }
            avgArray[i] = sum / k;
        }

        return avgArray;
    }
}
